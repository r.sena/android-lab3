package course.labs.activitylab;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

    // string for logcat documentation
    private final static String TAG = "Lab-ActivityOne";


    // lifecycle counts
    private int countOnCreate = 0, countOnStart = 0, countOnResume = 0;
    private int countOnDestroy = 0, countOnPause = 0, countOnStop = 0, countOnRestart = 0;

    private SharedPreferences prefs;
    private TextView display;
    //Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
    // You will need to increment these variables' values when their corresponding lifecycle methods get called.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);

        createPrefs();
        //Log cat print out
        Log.i(TAG, "onCreate called");

        //TODO:
        countOnCreate++;
        display = (TextView) findViewById(R.id.create);
        display.setText("onCreate(): " + countOnCreate);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_one, menu);
        return true;
    }

    // lifecycle callback overrides

    @Override
    public void onStart() {
        super.onStart();

        //Log cat print out
        Log.i(TAG, "onStart called");

        //TODO:
        //update the appropriate count variable
        //update the view
        countOnStart++;
        display = (TextView) findViewById(R.id.start);
        display.setText("onStart(): " + countOnStart);

    }

    // TODO: implement 5 missing lifecycle callback methods

    @Override
    public void onResume()
    {
        super.onResume();

        //Log cat print out
        Log.i(TAG, "onResume called");

        //TODO:
        //update the appropriate count variable
        //update the view
        countOnResume++;
        display = (TextView) findViewById(R.id.resume);
        display.setText("onResume(): " + countOnResume);
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();

        //Log cat print out
        Log.i(TAG, "onDestroy called");

        //TODO:
        //update the appropriate count variable
        //update the view
        countOnDestroy++;
        display = (TextView) findViewById(R.id.destroy);
        display.setText("onDestroy(): " + countOnDestroy);
    }

    @Override
    public void onPause()
    {
        super.onPause();

        //Log cat print out
        Log.i(TAG, "onPause called");

        //TODO:
        //update the appropriate count variable
        //update the view
        countOnPause++;
        display = (TextView) findViewById(R.id.pause);
        display.setText("onPause(): " + countOnPause);
    }

    @Override
    public void onStop()
    {
        super.onStop();

        //Log cat print out
        Log.i(TAG, "onStop called");

        //TODO:
        //update the appropriate count variable
        //update the view
        countOnStop++;
        display = (TextView) findViewById(R.id.stop);
        display.setText("onStop(): " + countOnStop);

        saveShared();
    }

    @Override
    public void onRestart()
    {
        super.onRestart();

        //Log cat print out
        Log.i(TAG, "onRestart called");

        //TODO:
        //update the appropriate count variable
        //update the view
        countOnRestart++;
        display = (TextView) findViewById(R.id.restart);
        display.setText("onRestart(): " + countOnRestart);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        super.onSaveInstanceState(savedInstanceState);
        //TODO:
        savedInstanceState.putInt("countOnCreate",countOnCreate);
        savedInstanceState.putInt("countOnStart",countOnStart);
        savedInstanceState.putInt("countOnRestart",countOnRestart);
        savedInstanceState.putInt("countOnStop",countOnStop);
        savedInstanceState.putInt("countOnPause",countOnPause);
        savedInstanceState.putInt("countOnDestroy",countOnDestroy);
        savedInstanceState.putInt("countOnResume",countOnResume);


        // save state information with a collection of key-value pairs
        // save all  count variables
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        countOnCreate = savedInstanceState.getInt("countOnCreate");
        countOnStart = savedInstanceState.getInt("countOnStart");
        countOnRestart = savedInstanceState.getInt("countOnRestart");
        countOnStop = savedInstanceState.getInt("countOnStop");
        countOnPause = savedInstanceState.getInt("countOnPause");
        countOnDestroy = savedInstanceState.getInt("countOnDestroy");
        countOnResume = savedInstanceState.getInt("countOnResume");
    }

    public void launchActivityTwo(View view) {
        //TODO:
        // This function launches Activity Two.
        // Hint: use Context’s startActivity() method.

        Intent launchActivityTwo = new Intent(getApplicationContext(),ActivityTwo.class);
        startActivity(launchActivityTwo);
    }

    private void saveShared()
    {
        SharedPreferences.Editor editor = prefs.edit();

        editor.putInt("countOnCreate",countOnCreate);
        editor.putInt("countOnStart",countOnStart);
        editor.putInt("countOnRestart",countOnRestart);
        editor.putInt("countOnStop",countOnStop);
        editor.putInt("countOnPause",countOnPause);
        editor.putInt("countOnDestroy",countOnDestroy);
        editor.putInt("countOnResume",countOnResume);

        editor.commit();
    }

    private void createPrefs()
    {
        prefs = getPreferences(MODE_PRIVATE);

        countOnCreate = prefs.getInt("countOnCreate", 0);
        countOnStart = prefs.getInt("countOnStart",0);
        countOnRestart = prefs.getInt("countOnRestart",0);
        countOnStop = prefs.getInt("countOnStop",0);
        countOnPause = prefs.getInt("countOnPause",0);
        countOnDestroy = prefs.getInt("countOnDestroy",0);
        countOnResume = prefs.getInt("countOnResume",0);
    }
}
